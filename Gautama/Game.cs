﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;

namespace Gautama
{
    public class Game : Core
    {
        public Game() : base(768, 768, false)
        {
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            Window.AllowUserResizing = true;

            Scene mainScene = Scene.createWithDefaultRenderer(Color.LightSeaGreen);
            scene = mainScene;
        }
    }
}
